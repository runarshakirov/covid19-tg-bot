require('dotenv').config();
const { Telegraf } = require('telegraf');
const Markup = require('telegraf/markup');
const COUNTRIES_LIST = require('./constants');

const api = require('covid19-api');
const { resize } = require('telegraf/markup');

const bot = new Telegraf(process.env.BOT_TOKEN);
bot.start((ctx) =>
  ctx.reply(
    `
Привет ${ctx.message.from.first_name}!
Узнай статистику по Коронавирусу.
Введи на английском название страны и получи статистику.
Посмотреть весь список стран можно командой /help.
`,
    Markup.keyboard([
      ['US', 'Russia', 'Ukraine'],
      ['Belarus', 'Canada', 'Turkey'],
    ])
      .resize()
      .extra()
  )
);

bot.help((ctx) => ctx.reply(COUNTRIES_LIST));

bot.on('text', async (ctx) => {
  let data = {};

  try {
    data = await api.getReportsByCountries(ctx.message.text);

    const formatData = `
Страна: ${data[0][0].country.charAt(0).toUpperCase() + data[0][0].country.substr(1)}
Случаи: ${data[0][0].cases}
Смертей: ${data[0][0].deaths}
Вылечились: ${data[0][0].recovered}
  `;
    ctx.reply(formatData);
  } catch {
    ctx.reply('Ошибка, такой страны не существует! Посмотрите /help.');
  }
});
bot.launch();
console.log('Бот запущен');
